#==============================================================================
# PROGRAM: hf_spectralanalysis.py
# AUTHOR: Henning Franke (m300738)
# CREATION: 10.12.2020
# SHORT DESCRIPTION: This module contains scripts for spectral analysis.
#------------------------------------------------------------------------------


#==============================================================================
#-- IMPORT PACKAGES
#------------------------------------------------------------------------------
#-- Import packages for calculations
import xarray       as xr
import dask.array   as dr
import numpy        as np
import scipy        as sp
import scipy.signal as sig
import scipy.fft    as fft
from sklearn.linear_model import LinearRegression as sklinreg
import dask

#-- Import plotting packages
# import matplotlib.pyplot as plt
# import matplotlib.colors as colors
# import matplotlib.dates  as mdates
# import seaborn           as sns

#-- Import packages for data and path handling
from datetime import datetime
from pathlib import Path
import sys
sys.path.append(str(Path.home() / Path("python/lib")))
import gc

#-- Import self-written packages
#import hf_definitions as hfdef
import hf_functions   as hffun
#import hf_plotting    as hfplt



#==============================================================================
# function expand_wk_fft_sets(): Expands the input FFT settings for the
#                                common Wheeler and Kiladis (1999) anaysis.
#------------------------------------------------------------------------------
def expand_wk_fft_sets(nDayTot, fft_specs):
    #==========================================================================
    #  S E T   B A S I C   S P E C T R A L   A N A L Y S I S   S E T T I N G S
    #--------------------------------------------------------------------------
    
    #-- Rewriting input setting parameter
    var        = fft_specs['var']
    dimension  = fft_specs['dimension']
    lat_range  = fft_specs['lat_range']
    spd        = fft_specs['spd']
    nDayWin    = fft_specs['nDayWin']
    nDaySkp    = fft_specs['nDaySkp']
    taper_name = fft_specs['taper_name']
    norm       = fft_specs['norm']
    
    #-- Calculate WK parameters out of input parameters
    nSamTot = nDayTot * spd                   # Total number of samples in dataset
    nSamWin = nDayWin * spd                   # Number of samples per window
    nSamSkp = nDaySkp * spd                   # Number of samples to skip between windows
    nWindow = int((nSamTot-nSamWin)/(nSamWin+nSamSkp) + 1) #-- Total amount of windows

    #-- Shorten daily windows if sample is shorter than nDayWin
    if nDayTot < nDayWin:
        nDayWin = subset.dims['time']/spd

    #-- Critical frequency: Remove all contributions with a lower frequency
    fCrit = 1./nDayWin    
    
    #-- Rewriting for output
    fft_specs = {'var'        : var,
                 'dimension'  : dimension,
                 'lat_range'  : lat_range,
                 'spd'        : spd,
                 'nDayWin'    : nDayWin,
                 'nDaySkp'    : nDaySkp,
                 'taper_name' : taper_name,
                 'norm'       : norm,
                 'nDayTot'    : nDayTot,
                 'nSamTot'    : nSamTot,
                 'nSamWin'    : nSamWin,
                 'nSamSkp'    : nSamSkp,
                 'nWindow'    : nWindow,
                 'nDayWin'    : nDayWin,
                 'fCrit'      : fCrit}
    
    return fft_specs


#==============================================================================
# function HF_fft(): Performs a FFT for 2-dimensional spatial fields. Depending
#                    on "dimension", Fourier coefficients will be calculated
#                    along one or two axes.
#------------------------------------------------------------------------------
def spc_analysis(fft_specs, subset, fft_outfile, power_outfile, mer_weight,
                 switch_smooth, n_smooth):
    #==========================================================================
    #  S E T   B A S I C   S P E C T R A L   A N A L Y S I S   S E T T I N G S
    #--------------------------------------------------------------------------
    #-- Print start time
    print('-------------------------------------------------------------------------------------')
    print('\033[1m\033[92m Start:     ' + datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    print('')
    
    #-- Rewriting input setting parameter
    var        = fft_specs['var']
    dimension  = fft_specs['dimension']
    lat_range  = fft_specs['lat_range']
    spd        = fft_specs['spd']
    nDayWin    = fft_specs['nDayWin']
    nDaySkp    = fft_specs['nDaySkp']
    taper_name = fft_specs['taper_name']
    norm       = fft_specs['norm']
    
    #-- Calculate WK parameters out of input parameters
    nDayTot = subset.sizes['time']/spd        # Total number of days in dataset
    nSamTot = nDayTot * spd                   # Total number of samples in dataset
    nSamWin = nDayWin * spd                   # Number of samples per window
    nSamSkp = nDaySkp * spd                   # Number of samples to skip between windows
    nWindow = int((nSamTot-nSamWin)/(nSamWin+nSamSkp) + 1) #-- Total amount of windows

    #-- Shorten daily windows if sample is shorter than nDayWin
    if nDayTot < nDayWin:
        nDayWin = subset.dims['time']/spd

    #-- Critical frequency: Remove all contributions with a lower frequency
    fCrit = 1./nDayWin
    
    #-- Print spectral analysis settings
    print(f'\033[0m nDayTot = \033[1m\033[91m{nDayTot}')
    print(f'\033[0m nSamTot = \033[1m\033[91m{nSamTot}')
    print(f'\033[0m nDayWin = \033[1m\033[91m{nDayWin}')
    print(f'\033[0m nSamWin = \033[1m\033[91m{nSamWin}')
    print(f'\033[0m nDaySkp = \033[1m\033[91m{nDaySkp}')
    print(f'\033[0m nSamSkp = \033[1m\033[91m{nSamSkp}')
    print(f'\033[0m nWindow = \033[1m\033[91m{nWindow}')
    print(f'\033[0m fCrit   = \033[1m\033[91m{fCrit}')
    print(f'\033[0m taper   = \033[1m\033[91m{taper_name}')
    print(f'\033[0m norm    = \033[1m\033[91m{norm}')
    print('')
    
    
    #============================================================================
    #                            D E T R E N D I N G
    #----------------------------------------------------------------------------
    #-- Remove LONG TERM linear trend in whole dataset. But KEEP the mean!
    attrs  = subset.attrs
    #subset = subset.reduce(sig.detrend, axis=0, type='linear',
    #                       keep_attrs=True) + \
    #                       subset.mean(dim='time', keep_attrs=True)
    subset.attrs = attrs

    #-- Remove annual cyle if dataset is longer than two years
    if nDayTot >= 365.:
        # TBD: some function to remove annual cycle...
        print("Currently a function to remove annual cycle doesn't exists! :(")
    
    
    #============================================================================
    #           I N I T I A L I Z E   O U T P U T   A R R A Y s
    #----------------------------------------------------------------------------
    #-- 2dim FFT along frequency and zonal wavenumber
    if dimension == 'fk':
        #                        F F T _  C O E F F
        #------------------------------------------------------------------------
        fft_coeff = xr.DataArray(data = dr.from_array(np.zeros((nSamWin,
                                                                subset.sizes['lat'],
                                                                subset.sizes['lon']),
                                                               dtype=complex)),
                                 coords = {'f_fft' : np.concatenate([np.arange(0, nSamWin/2+1),
                                                                     np.arange(-nSamWin/2+1, 0)]),
                                           'lat'   : subset.lat,
                                           'k_fft' : np.concatenate([np.arange(0, subset.sizes['lon']/2+1),
                                                                     np.arange(-subset.sizes['lon']/2+1, 0)])},
                                 dims = ('f_fft', 'lat', 'k_fft'),
                                 name = f'fft_coeff_{var}')
        
        #-- Set array attributes
        fft_coeff['f_fft'].attrs['standard_name'] = 'wave frequency'
        fft_coeff['f_fft'].attrs['long_name'] = 'wave frequency'
        fft_coeff['f_fft'].attrs['units'] = '--'
        fft_coeff['f_fft'].attrs['axis'] = 'T'

        fft_coeff['k_fft'].attrs['standard_name'] = 'zonal wave number'
        fft_coeff['k_fft'].attrs['long_name'] = 'zonal wave number'
        fft_coeff['k_fft'].attrs['units'] = '--'
        fft_coeff['k_fft'].attrs['axis'] = 'X'
        
        
        #                       P O W E R _  A V G  
        #-------------------------------------------------------------------------
        power_avg = xr.DataArray(data = dr.from_array(np.zeros((nSamWin,
                                                                subset.sizes['lat'],
                                                                subset.sizes['lon']))),
                                 coords = {'f_fft' : np.concatenate([np.arange(0, nSamWin/2+1),
                                                                     np.arange(-nSamWin/2+1, 0)]),
                                           'lat'   : subset.lat,
                                           'k_fft' : np.concatenate([np.arange(0, subset.sizes['lon']/2+1),
                                                                     np.arange(-subset.sizes['lon']/2+1, 0)])},
                                 dims = ('f_fft', 'lat', 'k_fft'),
                                 name = 'spc_power_squ_avg_fft')

        power_avg['f_fft'].attrs['standard_name'] = 'wave frequency'
        power_avg['f_fft'].attrs['long_name'] = 'wave frequency'
        power_avg['f_fft'].attrs['units'] = '--'
        power_avg['f_fft'].attrs['axis'] = 'T'

        power_avg['k_fft'].attrs['standard_name'] = 'zonal wave number'
        power_avg['k_fft'].attrs['long_name'] = 'zonal wave number'
        power_avg['k_fft'].attrs['units'] = '--'
        power_avg['k_fft'].attrs['axis'] = 'X'
        
        
        #                              P O W E R
        #-------------------------------------------------------------------------
        power = xr.DataArray(
                             coords = {'k'  : np.arange(-fft_coeff.sizes['k_fft']/2,
                                                         fft_coeff.sizes['k_fft']/2+1),
                                       'lat': fft_coeff.lat,
                                       'f'  : np.arange(-fft_coeff.sizes['f_fft']/2,
                                                         fft_coeff.sizes['f_fft']/2+1)/nDayWin},
                             dims   = ('k', 'lat', 'f'),
                             name   = f'power_{var}')

        #-- Set array attributes
        power['f'].attrs['standard_name'] = 'wave frequency'
        power['f'].attrs['long_name'] = 'wave frequency'
        power['f'].attrs['units'] = 'cpd'
        power['f'].attrs['axis'] = 'T'

        power['k'].attrs['standard_name'] = 'zonal wave number'
        power['k'].attrs['long_name'] = 'zonal wave number'
        power['k'].attrs['units'] = '1'
        power['k'].attrs['axis'] = 'X'
    
        print("------------------------")
        print(fft_coeff.k_fft.data)
        print(fft_coeff.k_fft)
        
    
    #-- 1dim FFT along frequency
    elif dimension == 'f':
        #                        F F T _  C O E F F
        #------------------------------------------------------------------------
        fft_coeff = xr.DataArray(data = dr.from_array(np.zeros((nSamWin,
                                                                subset.sizes['lat'],
                                                                subset.sizes['lon']),
                                                               dtype=complex)),
                                 coords = {'f_fft' : np.concatenate([np.arange(0, nSamWin/2+1),
                                                                     np.arange(-nSamWin/2+1, 0)]),
                                           'lat'   : subset.lat,
                                           'lon'   : subset.lon},
                                 dims = ('f_fft', 'lat', 'lon'),
                                 name = f'fft_coeff_{var}')

        #-- Set array attributes
        fft_coeff['f_fft'].attrs['standard_name'] = 'wave frequency'
        fft_coeff['f_fft'].attrs['long_name'] = 'wave frequency'
        fft_coeff['f_fft'].attrs['units'] = '--'
        fft_coeff['f_fft'].attrs['axis'] = 'T'
        
        
        #                        P O W E R _  A V G 
        #------------------------------------------------------------------------
        power_avg = xr.DataArray(data = dr.from_array(np.zeros((nSamWin,
                                                                subset.sizes['lat'],
                                                                subset.sizes['lon']))),
                                 coords = {'f_fft' : np.concatenate([np.arange(0, nSamWin/2+1),
                                                                     np.arange(-nSamWin/2+1, 0)]),
                                           'lat'   : subset.lat,
                                           'lon'   : subset.lon},
                                 dims = ('f_fft', 'lat', 'lon'),
                                 name = 'spc_power_squ_avg_fft')

        power_avg['f_fft'].attrs['standard_name'] = 'wave frequency'
        power_avg['f_fft'].attrs['long_name'] = 'wave frequency'
        power_avg['f_fft'].attrs['units'] = '--'
        power_avg['f_fft'].attrs['axis'] = 'T'
        
        
        #                            P O W E R
        #------------------------------------------------------------------------
        power = xr.DataArray(data = np.zeros([int(fft_coeff.sizes['lon'   ]    ),
                                              int(fft_coeff.sizes['lat'   ]    ),
                                              int(fft_coeff.sizes['f_fft' ]/2+1)]),
                             coords = {'lon': fft_coeff.lon,
                                       'lat': fft_coeff.lat,
                                       'f'  : np.arange(0, fft_coeff.sizes['f_fft']/2+1) / nDayWin},
                             dims   = ('lon', 'lat', 'f'),
                             name   = f'power_{var}')

        #-- Set array attributes
        power['f'].attrs['standard_name'] = 'wave frequency'
        power['f'].attrs['long_name'] = 'wave frequency'
        power['f'].attrs['units'] = 'cpd'
        power['f'].attrs['axis'] = 'T'
                             
    
    #-- 1dim FFT along zonal wavenumber
    elif dimension == 'k':
        #                        F F T _  C O E F F
        #------------------------------------------------------------------------
        fft_coeff = xr.DataArray(data = dr.from_array(np.zeros((nSamWin,
                                                                subset.sizes['lat'],
                                                                subset.sizes['lon']),
                                                               dtype=complex)),
                                 coords = {'time'  : subset.time,
                                           'lat'   : subset.lat,
                                           'k_fft' : np.concatenate([np.arange(0, subset.sizes['lon']/2+1),
                                                                     np.arange(-subset.sizes['lon']/2+1, 0)])},
                                 dims = ('time', 'lat', 'k_fft'),
                                 name = f'fft_coeff_{var}')

        #-- Set array attributes
        fft_coeff['k_fft'].attrs['standard_name'] = 'zonal wave number'
        fft_coeff['k_fft'].attrs['long_name'] = 'zonal wave number'
        fft_coeff['k_fft'].attrs['units'] = '--'
        fft_coeff['k_fft'].attrs['axis'] = 'X'
        
        
        #                        P O W E R _  A V G 
        #------------------------------------------------------------------------
        power_avg = xr.DataArray(data = dr.from_array(np.zeros((nSamWin,
                                                                subset.sizes['lat'],
                                                                subset.sizes['lon']))),
                                 coords = {'time'  : subset.time,
                                           'lat'   : subset.lat,
                                           'k_fft' : np.concatenate([np.arange(0, subset.sizes['lon']/2+1),
                                                                     np.arange(-subset.sizes['lon']/2+1, 0)])},
                                 dims = ('time', 'lat', 'k_fft'),
                                 name = 'spc_power_squ_avg_fft')

        power_avg['k_fft'].attrs['standard_name'] = 'zonal wave number'
        power_avg['k_fft'].attrs['long_name'] = 'zonal wave number'
        power_avg['k_fft'].attrs['units'] = '--'
        power_avg['k_fft'].attrs['axis'] = 'X'
        
        
        #                            P O W E R
        #------------------------------------------------------------------------
        power = xr.DataArray(data = np.zeros([int(fft_coeff.sizes['k_fft' ]/2+1),
                                              int(fft_coeff.sizes['lat'   ]),
                                              int(fft_coeff.sizes['time'  ])],
                                             dtype=complex),
                             coords = {'k'   : np.arange(0, fft_coeff.sizes['k_fft']/2+1),
                                       'lat' : fft_coeff.lat,
                                       'time': fft_coeff.time},
                             dims   = ('k', 'lat', 'time'),
                             name   = f'power_{var}')

        #-- Set array attributes
        power['k'].attrs['standard_name'] = 'zonal wave number'
        power['k'].attrs['long_name'] = 'zonal wave number'
        power['k'].attrs['units'] = '1'
        power['k'].attrs['axis'] = 'X'
    
    
    #-- Set global array attributes
    fft_coeff.attrs['spd']     = str(spd)
    fft_coeff.attrs['nDayWin'] = str(nDayWin)
    fft_coeff.attrs['nDaySkp'] = str(nDaySkp)
    fft_coeff.attrs['taper']   = taper_name
    fft_coeff.attrs['norm']    = str(norm)
    
    power_avg.attrs['spd']       = str(spd)
    power_avg.attrs['nDayWin']   = str(nDayWin)
    power_avg.attrs['nDaySkp']   = str(nDaySkp)
    power_avg.attrs['taper']     = taper_name
    power_avg.attrs['norm']      = str(norm)
    power_avg.attrs['long_name'] = 'spectral power (not Hayashi decomposed)'
    
    power.attrs['spd']       = str(spd)
    power.attrs['nDayWin']   = str(nDayWin)
    power.attrs['nDaySkp']   = str(nDaySkp)
    power.attrs['taper']     = taper_name
    power.attrs['norm']      = str(norm)
    power.attrs['long_name'] = 'spectral power'
    
    
    #============================================================================
    #               P E R F O R M   F F T   A N A L Y S I S
    #----------------------------------------------------------------------------
    #-- Initialize taper
    taper = init_taper(subset, nSamWin, taper_name)
    
    #-- Set start and end time
    t_start = 0
    t_end   = nDayWin * spd

    #-- Loop over windows
    if dimension == 'fk':
        for nw in np.arange(0, nWindow):
            #-- Print statement
            print(f'\033[1mFFT for window {str(nw)}!')
            print('')
            
            if not Path(str(fft_outfile) + f'_window{str(nw).zfill(2)}.nc').exists():
                #-- Create working files
                workset = subset.isel(time = slice(t_start, t_end))
            
                print("Sum: ", sum(sum(sum(np.asarray(workset.data)))))

                #-- Temporal detrending
                print("Shpae is: ", workset.shape)
                print("Type is: ", type(workset))
                #workset = workset.reduce(sig.detrend, axis=0, type='linear')
                print("Shpae is: ", workset.shape)
                print("Type is: ", type(workset))
                #exit()

                #-- Tapering
                #workset.data = workset.data * taper.data
                workset.data = np.asarray(workset.data * taper.data)
                print("Sum: ", sum(sum(sum(np.asarray(workset.data)))))

                #-- 2d FFT
                if norm:
                    fft_coeff.data = dr.from_array(fft.fft2(workset.data, axes=[0, 2]) / \
                                                                   (workset.sizes['lon'] * nSamWin))
                elif not norm:
                    fft_coeff.data = dr.from_array(fft.fft2(workset.data, axes=[0, 2]))
                    print("Sum: ", sum(sum(sum(np.asarray(fft_coeff.data)))))
                    
                #-- Save Fourier coefficients
                hffun.save_complex(fft_coeff, path=f'{str(fft_outfile)}_window{str(nw).zfill(2)}.nc')
            
            else:
                fft_coeff = hffun.read_complex(f'{str(fft_outfile)}_window{str(nw).zfill(2)}.nc')
                fft_coeff = fft_coeff[f'fft_coeff_{var}']
                
            #-- Average fft coefficients and calculate spectral power
            power_avg.data = power_avg.data + 1/nWindow * (np.real(fft_coeff)**2 + np.imag(fft_coeff)**2)
            
            #-- Set start and end index for next window
            t_start = t_end   + nSamSkp
            t_end   = t_start + nSamWin
            
            #-- Clean garbage collection
            gc.collect()
        
        #-- Transpose power_avg
        power_avg = power_avg.transpose('k_fft', 'lat', 'f_fft')

        print("####################")
        print("####################")
        print(power_avg.f_fft.data)
        print(power_avg.f_fft.shape)
        
        #-- Save squared and averaged Fourier coefficients
        power_avg.squeeze().to_netcdf(f'{str(power_outfile)}_squ_avg_fft.nc')
        #hffun.save_complex(power_avg, path=f'{str(power_outfile)}_squ_avg_fft.nc')
    
    elif dimension == 'f':
        for nw in np.arange(0, nWindow):
            #-- Print statement
            print(f'\033[1mFFT for window {str(nw)}!')
            print('')
            
            if not Path(str(fft_outfile) + f'_window{str(nw).zfill(2)}.nc').exists():
                #-- Create working files
                workset = subset.isel(time = slice(t_start, t_end))

                #-- Temporal detrending
                workset = workset.reduce(sig.detrend, axis=0, type='linear')

                #-- Tapering
                workset.data = workset.data * taper.data

                #-- 1d FFT
                if norm:
                    fft_coeff.data = dr.from_array(fft.fft(workset.data, axis=0) / nSamWin)
                elif not norm:
                    fft_coeff.data = dr.from_array(fft.fft(workset.data, axis=0))

                #-- Save Fourier coefficients
                hffun.save_complex(fft_coeff, path=f'{str(fft_outfile)}_window{str(nw).zfill(2)}.nc')
            
            else:
                fft_coeff = hffun.read_complex(f'{str(fft_outfile)}_window{str(nw).zfill(2)}.nc')
                fft_coeff = fft_coeff[f'fft_coeff_{var}']
                
            #-- Average fft coefficients and calculate spectral power
            power_avg.data = power_avg.data + 1/nWindow * (np.real(fft_coeff)**2 + np.imag(fft_coeff)**2)
            
            #-- Set start and end index for next window
            t_start = t_end   + nSamSkp
            t_end   = t_start + nSamWin
            
            #-- Clean garbage collection
            gc.collect()
        
        #-- Transpose power_avg
        power_avg = power_avg.transpose('lon', 'lat', 'f_fft')
        
        #-- Save squared and averaged Fourier coefficients
        hffun.save_complex(power_avg, path=f'{str(power_outfile)}_squ_avg_fft.nc')
        
        
    elif dimension == 'k':
        for nw in np.arange(0, nWindow):
            #-- Print statement
            print(f'\033[1mFFT for window {str(nw)}!')
            print('')
                             
            if not Path(str(fft_outfile) + f'_window{str(nw).zfill(2)}.nc').exists():
                #-- Create working files
                workset = subset.isel(time = slice(t_start, t_end))

                #-- Temporal detrending
                workset = workset.reduce(sig.detrend, axis=0, type='linear')

                #-- 1d FFT
                if norm:
                    fft_coeff.data = dr.from_array(fft.fft(workset.data, axis=2) / workset.sizes['lon'])
                elif not norm:
                    fft_coeff.data = dr.from_array(fft.fft(workset.data, axis=2))

                #-- Save Fourier coefficients
                hffun.save_complex(fft_coeff, path=f'{str(fft_outfile)}_window{str(nw).zfill(2)}.nc')
                
            else:
                fft_coeff = hffun.read_complex(f'{str(fft_outfile)}_window{str(nw).zfill(2)}.nc')
                fft_coeff = fft_coeff[f'fft_coeff_{var}']
                
            #-- Average fft coefficients and calculate spectral power
            power_avg.data = power_avg.data + 1/nWindow * (np.real(fft_coeff)**2 + np.imag(fft_coeff)**2)
            
            #-- Set start and end index for next window
            t_start = t_end   + nSamSkp
            t_end   = t_start + nSamWin
            
            #-- Clean garbage collection
            gc.collect()
        
        #-- Transpose power_avg
        power_avg = power_avg.transpose('k_fft', 'lat', 'time')
        
        #-- Save squared and averaged Fourier coefficients
        hffun.save_complex(power_avg, path=f'{str(power_outfile)}_squ_avg_fft.nc')
            
    
    #==========================================================================
    #       C A L C U L A T E   P O W E R   S P E C T R U M
    #--------------------------------------------------------------------------
    if dimension == 'fk':
        #-- Set indexing variables for faster access
        mlon      = int(fft_coeff.sizes['k_fft'])
        mlon_2    = int(mlon/2)
        nSamWin_2 = int(nSamWin/2)

        print("mlon, mlon_2, nSamWin, nSamWin_2, power_avg.shape")
        print(mlon)
        print(mlon_2)
        print(nSamWin)
        print(nSamWin_2)
        print(power_avg.shape)
        
        #-- Decompose spectra according to Hayashi
        #power.data[:mlon_2 , :, :nSamWin_2  ] = power_avg[ mlon_2:0:-1, :, nSamWin_2:            ]
        power.data[:mlon_2 , :, nSamWin_2:  ] = power_avg[ mlon_2:0:-1, :, :nSamWin_2+1          ]
        #power.data[ mlon_2:, :, :nSamWin_2+1] = power_avg[:mlon_2+1   , :, nSamWin_2::-1         ]
        power.data[ mlon_2:, :, nSamWin_2+1:] = power_avg[:mlon_2+1   , :, nSamWin:nSamWin_2-1:-1]

        #power.data[ :mlon, :, nSamWin_2+1:] = power_avg[:, :, nSamWin:nSamWin_2-1:-1] #ok
        #power.data[ mlon_2:, :, :nSamWin] = power_avg[:mlon_2+1   , :, :]

        #power.data[:mlon, :, :nSamWin] = power_avg[:,:,:]
        
        print("SUM ABC")
        print(np.sum(np.array(power)))

        #-- Transfer to dask array
        power.data = dr.from_array(power.data)
        
        #-- Remove negative frequencies and set the mean (f=0) to missing
        power = power.where(power.f >= 0, drop=True)
        #power = power.where(power.f != 0.0, np.NaN)
        power = power.where(power.f != 0.0, 0.0)        
            
    elif dimension == 'f':
        #-- Transfer to dask array
        power.data = dr.from_array(power.data)
        
        #-- Drop negative frequencies
        power.data = power_avg.where(fft_coeff.f_fft >= 0, drop=True)
        power = power.where(power.f != 0.0, np.NaN)
    
    elif dimension == 'k':
        #-- Transfer to dask array
        power.data = dr.from_array(power.data)
        
        #-- Drop negative frequencies
        power.data = power_avg.where(fft_coeff.k_fft >= 0, drop=True)
        power = power.where(power.k != 0.0, np.NaN)

    #-- Weighted meridional mean
    power.data = power.data * mer_weight
    power = power.mean(dim='lat')
    
    #-- Further averaging for 1d power spectra
    if dimension == 'f':
        power = power.mean(dim='lon')
    
    elif dimension == 'k':
        power = power.mean(dim='time')

    
    #==========================================================================
    #                    W K - 1 2 1   S M O O T H I N G 
    #--------------------------------------------------------------------------
    if switch_smooth:
        #----------------------------------------------------------------------
        #                          SMOOTHING IN k
        #-- Settings for smoothing of background spectrum
        smoothed = {str(n_smooth['k'][0]): power.where((power['f']  < 0.1), drop=True),
                    str(n_smooth['k'][1]): power.where((power['f'] >= 0.1) &
                                                  (power['f']  < 0.2), drop=True),
                    str(n_smooth['k'][2]): power.where((power['f'] >= 0.2) &
                                                  (power['f']  < 0.3), drop=True),
                    str(n_smooth['k'][3]): power.where((power['f'] >= 0.3), drop=True)}

        #-- Smoothing of background spectrum
        for N in n_smooth['k']:
            i_smooth = 0 
            while i_smooth < N:
                smoothed[str(N)] = wk121(smoothed[str(N)], dim='k')
                i_smooth = i_smooth + 1

        #-- Merge smoothed dataset back together
        smoothed = xr.concat([smoothed[str(N)] for N in n_smooth], dim='f')

        #----------------------------------------------------------------------
        #                          SMOOTHING IN f
        #-- Smoothing of background spectrum
        zero     = smoothed.where((smoothed['f'] == 0.0), drop=True)
        tbsmooth = smoothed.where((smoothed['f'] != 0.0), drop=True)
        for n in range(0, n_smooth['f']):
            tbsmooth = wk121(tbsmooth, dim='f')
        
        smoothed = xr.concat([zero, tbsmooth], dim='f')
        #----------------------------------------------------------------------
        
        #-- Persist
        power = smoothed.chunk('auto').persist()
        
    else:
        #-- Persist
        power = power.chunk('auto').persist()
    
    #-- Set global attributes
    power.attrs['spd']       = str(spd)
    power.attrs['nDayWin']   = str(nDayWin)
    power.attrs['nDaySkp']   = str(nDaySkp)
    power.attrs['taper']     = taper_name
    power.attrs['norm']      = str(norm)
    power.attrs['lat_range'] = str(lat_range)
    power.attrs['smoothing'] = str(switch_smooth)
    power.attrs['n_smooth']  = str(n_smooth)  
    
    #-- Save power spectrum
    power.squeeze().to_netcdf(path=f'{str(power_outfile)}.nc')
    
    #==========================================================================
    #                         E N D   F U N C T I O N
    #--------------------------------------------------------------------------
    #-- Print start time
    print('\033[1m\033[92m End:       ' + datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    print('\033[0m-------------------------------------------------------------------------------------')
    print('')


#==============================================================================
# function wk_analysis(): Performs a 2-dimensional FFT based on the common
#                         Wheeler and Kiladis (1999) preprocessing for 2-dimen-
#                         sional spatial fields (e.g. precipitation flux).
#------------------------------------------------------------------------------
def wk_analysis(fft_specs, dtrend, subset, fft_outfile, power_outfile,
                mer_weight, switch_smooth, n_smooth):
    #==========================================================================
    #  S E T   B A S I C   S P E C T R A L   A N A L Y S I S   S E T T I N G S
    #--------------------------------------------------------------------------
    #-- Print start time
    print('-------------------------------------------------------------------------------------')
    print('\033[1m\033[92m Start:     ' + datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    print('')
    
    #-- Rewriting input setting parameter
    var        = fft_specs['var']
    lat_range  = fft_specs['lat_range']
    spd        = fft_specs['spd']
    nDayWin    = fft_specs['nDayWin']
    nDaySkp    = fft_specs['nDaySkp']
    taper_name = fft_specs['taper_name']
    norm       = fft_specs['norm']
    
    #-- Calculate WK parameters out of input parameters
    nDayTot = subset.sizes['time']/spd        # Total number of days in dataset
    nSamTot = nDayTot * spd                   # Total number of samples in dataset
    nSamWin = nDayWin * spd                   # Number of samples per window
    nSamSkp = nDaySkp * spd                   # Number of samples to skip between windows
    nWindow = int((nSamTot-nSamWin)/(nSamWin+nSamSkp) + 1) #-- Total amount of windows
    
    #-- Shorten daily windows if sample is shorter than nDayWin
    if nDayTot < nDayWin:
        nDayWin = subset.dims['time']/spd
    
    #-- Critical frequency: Remove all contributions with a lower frequency
    fCrit = 1./nDayWin
    
    #-- Print spectral analysis settings
    print(f'\033[0m nDayTot = \033[1m\033[91m{nDayTot}')
    print(f'\033[0m nSamTot = \033[1m\033[91m{nSamTot}')
    print(f'\033[0m nDayWin = \033[1m\033[91m{nDayWin}')
    print(f'\033[0m nSamWin = \033[1m\033[91m{nSamWin}')
    print(f'\033[0m nDaySkp = \033[1m\033[91m{nDaySkp}')
    print(f'\033[0m nSamSkp = \033[1m\033[91m{nSamSkp}')
    print(f'\033[0m nWindow = \033[1m\033[91m{nWindow}')
    print(f'\033[0m fCrit   = \033[1m\033[91m{fCrit}')
    print(f'\033[0m taper   = \033[1m\033[91m{taper_name}')
    print(f'\033[0m norm    = \033[1m\033[91m{norm}')
    print('')
    
    
    #============================================================================
    #                            D E T R E N D I N G
    #----------------------------------------------------------------------------
    #-- Remove LONG TERM linear trend in whole dataset. But KEEP the mean!
    attrs  = subset.attrs
    subset = subset.reduce(sig.detrend, axis=0, type='linear',
                           keep_attrs=True) + \
                           subset.mean(dim='time', keep_attrs=True)
    subset.attrs = attrs

    #-- Remove annual cyle if dataset is longer than two years
    if nDayTot >= 365.:
        # TBD: some function to remove annual cycle...
        print("Currently a function to remove annual cycle doesn't exists! :(")
    
    
    #============================================================================
    # S P L I T   I N T O   ( A N T I ) S Y M M E T R I C   C O M P O N E N T S
    #----------------------------------------------------------------------------
    #-- Initialize dataset, which stores symmetric data in NH and antisymmetric
    #   data in SH
    symset = subset - subset.mean(dim=['time', 'lon'])

    #-- Calculate symetric and antisymmetric subset
    n_lat2 = int(subset.sizes["lat"]/2)
    symset.data = np.concatenate((0.5*(symset.data[:, :n_lat2, :] +
                                       symset.data[:, :n_lat2-1:-1, :]),
                                  0.5*(symset.data[:, n_lat2-1::-1, :] -
                                       symset.data[:, n_lat2:, :])),
                                 axis=1)
    symset = symset + subset.mean(dim=['time', 'lon'])

    #-- Rechunking
    symset = symset.chunk('auto')

    #-- Set metadata for antisymmetric data
    symset.attrs['standard_name'] = f'as_{var}'
    symset.attrs['long_name'] = f'symmetric (NH) and antisymmetric (SH) {subset.attrs["long_name"]}'
    symset.attrs['units'] = subset.attrs['units']

    #-- Free memory
    del subset
    
    #============================================================================
    #           I N I T I A L I Z E   O U T P U T   A R R A Y S
    #----------------------------------------------------------------------------
    # F F T _  C O E F F
    #-------------------
    fft_coeff = xr.DataArray(data = dr.from_array(np.zeros((nSamWin,
                                                            symset.sizes['lat'],
                                                            symset.sizes['lon']),
                                                           dtype=complex)),
                             coords = {'f_fft' : np.concatenate([np.arange(0, nSamWin/2+1),
                                                                 np.arange(-nSamWin/2+1, 0)]),
                                       'lat'   : symset.lat,
                                       'k_fft' : np.concatenate([np.arange(0, symset.sizes['lon']/2+1),
                                                                 np.arange(-symset.sizes['lon']/2+1, 0)])},
                             dims = ('f_fft', 'lat', 'k_fft'),
                             name = f'fft_coeff_{var}')

    fft_coeff.attrs['spd']     = str(spd)
    fft_coeff.attrs['nDayWin'] = str(nDayWin)
    fft_coeff.attrs['nDaySkp'] = str(nDaySkp)
    fft_coeff.attrs['taper']   = taper_name
    fft_coeff.attrs['norm']    = str(norm)
    
    fft_coeff['f_fft'].attrs['standard_name'] = 'wave frequency'
    fft_coeff['f_fft'].attrs['long_name'] = 'wave frequency'
    fft_coeff['f_fft'].attrs['units'] = '--'
    fft_coeff['f_fft'].attrs['axis'] = 'T'

    fft_coeff['k_fft'].attrs['standard_name'] = 'zonal wave number'
    fft_coeff['k_fft'].attrs['long_name'] = 'zonal wave number'
    fft_coeff['k_fft'].attrs['units'] = '--'
    fft_coeff['k_fft'].attrs['axis'] = 'X'
    
    
    # P O W E R _  A V G
    #-------------------
    power_avg = xr.DataArray(data = dr.from_array(np.zeros((nSamWin,
                                                            symset.sizes['lat'],
                                                            symset.sizes['lon']))),
                             coords = {'f_fft' : np.concatenate([np.arange(0, nSamWin/2+1),
                                                                 np.arange(-nSamWin/2+1, 0)]),
                                       'lat'   : symset.lat,
                                       'k_fft' : np.concatenate([np.arange(0, symset.sizes['lon']/2+1),
                                                                 np.arange(-symset.sizes['lon']/2+1, 0)])},
                             dims = ('f_fft', 'lat', 'k_fft'),
                             name = 'spc_power_squ_avg_fft')
    
    power_avg.attrs['spd']       = str(spd)
    power_avg.attrs['nDayWin']   = str(nDayWin)
    power_avg.attrs['nDaySkp']   = str(nDaySkp)
    power_avg.attrs['taper']     = taper_name
    power_avg.attrs['norm']      = str(norm)
    power_avg.attrs['long_name'] = 'spectral power (not Hayashi decomposed)'
    
    power_avg['f_fft'].attrs['standard_name'] = 'wave frequency'
    power_avg['f_fft'].attrs['long_name'] = 'wave frequency'
    power_avg['f_fft'].attrs['units'] = '--'
    power_avg['f_fft'].attrs['axis'] = 'T'

    power_avg['k_fft'].attrs['standard_name'] = 'zonal wave number'
    power_avg['k_fft'].attrs['long_name'] = 'zonal wave number'
    power_avg['k_fft'].attrs['units'] = '--'
    power_avg['k_fft'].attrs['axis'] = 'X'
    
    
    # P O W E R 
    #----------
    power = xr.DataArray(data = (np.zeros([int(fft_coeff.sizes['k_fft']+1),
                                           int(fft_coeff.sizes['lat'  ]  ),
                                           int(fft_coeff.sizes['f_fft']+1)])),
                         coords = {'k'  : np.arange(-fft_coeff.sizes['k_fft']/2,
                                                     fft_coeff.sizes['k_fft']/2+1),
                                   'lat': fft_coeff.lat,
                                   'f'  : np.arange(-fft_coeff.sizes['f_fft']/2,
                                                     fft_coeff.sizes['f_fft']/2+1)/nDayWin},
                         dims   = ('k', 'lat', 'f'),
                         name   = f'power_{var}')
    
    power.attrs['spd']       = str(spd)
    power.attrs['nDayWin']   = str(nDayWin)
    power.attrs['nDaySkp']   = str(nDaySkp)
    power.attrs['taper']     = taper_name
    power.attrs['norm']      = str(norm)
    power.attrs['long_name'] = 'spectral power'
    
    power['f'].attrs['standard_name'] = 'wave frequency'
    power['f'].attrs['long_name'] = 'wave frequency'
    power['f'].attrs['units'] = 'cpd'
    power['f'].attrs['axis'] = 'T'

    power['k'].attrs['standard_name'] = 'zonal wave number'
    power['k'].attrs['long_name'] = 'zonal wave number'
    power['k'].attrs['units'] = '1'
    power['k'].attrs['axis'] = 'X'
    
    #-- Clean garbage collection
    gc.collect()
    
    
    #============================================================================
    #               P E R F O R M   F F T   A N A L Y S I S
    #----------------------------------------------------------------------------
    #-- Initialize taper
    taper = init_taper(symset, nSamWin, taper_name)
    
    #-- Set start and end time
    t_start = 0
    t_end   = nDayWin * spd
    
    #-- Loop over windows
    for nw in np.arange(0, nWindow):
        #-- Print statement
        print(f'\033[1mFFT for window {str(nw)}!')
        print('')
        
        if not Path(str(fft_outfile) + f'_window{str(nw).zfill(2)}.nc').exists():
            #-- Create working files
            workset = symset.isel(time = slice(t_start, t_end))

            #-- Temporal detrending
            if dtrend['temporal'][0]:
                if dtrend['temporal'][1]:
                    workset = workset.mean(dim='time') + workset.reduce(sig.detrend, axis=0,
                                                                        type='linear')

                elif not dtrend['temporal'][1]:
                    workset = workset.reduce(sig.detrend, axis=0, type='linear')

            #-- Zonal detrending
            if dtrend['zonal'][0]:
                if dtrend['zonal'][1]:
                    workset = workset.mean(dim='time') + workset.reduce(sig.detrend, axis=2,
                                                                        type='linear')

                elif not dtrend['zonal'][1]:
                    workset = workset.reduce(sig.detrend, axis=2, type='linear')

            #-- Tapering
            workset.data = workset.data * taper.data

            #-- 2d FFT
            if norm:
                fft_coeff.data = dr.from_array(fft.fft2(workset.values, axes=[0, 2]) / \
                                                               (workset.sizes['lon'] * nSamWin))
            elif not norm:
                fft_coeff.data = dr.from_array(fft.fft2(workset.values, axes=[0, 2]))


            #-- Save Fourier coefficients
            hffun.save_complex(fft_coeff, path=f'{str(fft_outfile)}_window{str(nw).zfill(2)}.nc')
        
        else:
            fft_coeff = hffun.read_complex(f'{str(fft_outfile)}_window{str(nw).zfill(2)}.nc')
            fft_coeff = fft_coeff[f'fft_coeff_{var}']
            
        #-- Average fft coefficients and calculate spectral power
        power_avg.data = power_avg.data + 1/nWindow * (np.real(fft_coeff)**2 + np.imag(fft_coeff)**2)
            
        #-- Set start and end index for next window
        t_start = t_end   + nSamSkp
        t_end   = t_start + nSamWin
        
        #-- Clean garbage collection
        gc.collect()
    
    #-- Transpose power_avg
    power_avg = power_avg.transpose('k_fft', 'lat', 'f_fft')
    
    #-- Save squared and averaged Fourier coefficients
    hffun.save_complex(power_avg, path=f'{str(power_outfile)}_squ_avg_fft.nc')
    
    
    #==========================================================================
    #  S P L I T   S P E C T R U M   A C C O R D .  H A Y A S H I   ( 1 9 7 1)
    #--------------------------------------------------------------------------
    #-- Set indexing variables for faster access
    mlon      = int(power_avg.sizes['k_fft'])
    mlon_2    = int(mlon/2)
    nSamWin_2 = int(nSamWin/2)

    power.data[:mlon_2 , :, :nSamWin_2  ] = power_avg[ mlon_2:0:-1, :, nSamWin_2:           ]
    power.data[:mlon_2 , :, nSamWin_2:  ] = power_avg[ mlon_2:0:-1, :, :nSamWin_2+1         ]
    power.data[ mlon_2:, :, :nSamWin_2+1] = power_avg[:mlon_2+1  , :, nSamWin_2::-1         ]
    power.data[ mlon_2:, :, nSamWin_2+1:] = power_avg[:mlon_2+1  , :, nSamWin:nSamWin_2-1:-1]
    
    #-- Transfer to dask array
    power.data = dr.from_array(power.data)
    
    #-- Free memory
    del power_avg
    
    #==========================================================================
    #  A V E R A G I N G   &   D E C O M P O S I T I O N 
    #--------------------------------------------------------------------------
    #-- Latitudinal weighting of power
    power = power * mer_weight

    #-- Splitting into symmetric and antisymmetric components and summing over all latitudes.
    #   Multiplying by 2 since we only sum over half the array.
    power_sym  = power.where(power.lat >  0).sum(dim='lat') * 2
    power_asym = power.where(power.lat <= 0).sum(dim='lat') * 2
    power_bg   = power.sum(dim='lat')

    #-- Set the mean to missing
    power_sym  = power_sym.where(power_sym['f'] != 0.0, np.NaN)
    power_asym = power_asym.where(power_asym['f'] != 0.0, np.NaN)
    power_bg   = power_bg.where(power_bg['f'] != 0.0, np.NaN)

    #-- Remove negative frequencies
    power_sym  = power_sym.where(power_sym['f'] >= 0, drop=True)
    power_asym = power_asym.where(power_asym['f'] >= 0, drop=True)
    power_bg   = power_bg.where(power_bg['f'] >= 0, drop=True)
    
    
    #==========================================================================
    #                    W K - 1 2 1   S M O O T H I N G 
    #--------------------------------------------------------------------------
    if switch_smooth:
        #----------------------------------------------------------------------
        #                          SMOOTHING IN k
        #-- Smoothing of symmetric and antisymmetric spectrum
        smoothed_sym  = wk121(power_sym, dim='k')
        smoothed_asym = wk121(power_asym, dim='k')
        
        #-- Settings for smoothing of background spectrum
        smoothed_bg = {str(n_smooth['k'][0]): power_bg.where((power_bg['f']  < 0.1), drop=True),
                       str(n_smooth['k'][1]): power_bg.where((power_bg['f'] >= 0.1) &
                                                             (power_bg['f']  < 0.2), drop=True),
                       str(n_smooth['k'][2]): power_bg.where((power_bg['f'] >= 0.2) &
                                                             (power_bg['f']  < 0.3), drop=True),
                       str(n_smooth['k'][3]): power_bg.where((power_bg['f'] >= 0.3), drop=True)}

        #-- Smoothing of background spectrum
        for N in n_smooth['k']:
            i_smooth = 0 
            while i_smooth < N:
                smoothed_bg[str(N)] = wk121(smoothed_bg[str(N)], dim='k')
                i_smooth = i_smooth + 1

        #-- Merge smoothed dataset back together
        smoothed_bg = xr.concat([smoothed_bg[str(N)] for N in n_smooth['k']], dim='f')
        
        #-- Rechunk
        smoothed_bg   = smoothed_bg.chunk('auto')
        smoothed_sym  = smoothed_sym.chunk('auto')
        smoothed_asym = smoothed_asym.chunk('auto')
        
        #----------------------------------------------------------------------
        #                          SMOOTHING IN f
        #-- Smoothing of symmetric spectrum
        zero     = smoothed_sym.where((smoothed_sym['f'] == 0.0), drop=True)
        tbsmooth = wk121(smoothed_sym.where((smoothed_sym['f'] != 0.0), drop=True),
                         dim='f')
        smoothed_sym = xr.concat([zero, tbsmooth], dim='f')
        
        #-- Smoothing of antisymmetric spectrum
        zero     = smoothed_asym.where((smoothed_asym['f'] == 0.0), drop=True)
        tbsmooth = wk121(smoothed_asym.where((smoothed_asym['f'] != 0.0), drop=True),
                         dim='f')
        smoothed_asym = xr.concat([zero, tbsmooth], dim='f')
        
        #-- Smoothing of background spectrum
        zero     = smoothed_bg.where((smoothed_bg['f'] == 0.0), drop=True)
        tbsmooth = smoothed_bg.where((smoothed_bg['f'] != 0.0), drop=True)
        for n in range(0, n_smooth['f']):
            tbsmooth = wk121(tbsmooth, dim='f')
        
        smoothed_bg = xr.concat([zero, tbsmooth], dim='f')
        
        #----------------------------------------------------------------------
        
        #-- Persist
        smoothed_bg   = smoothed_bg.chunk('auto').persist()
        smoothed_sym  = smoothed_sym.chunk('auto').persist()
        smoothed_asym = smoothed_asym.chunk('auto').persist()
        
        #-- Merge to one power array
        power = xr.merge([smoothed_bg.rename('bg'),
                          smoothed_sym.rename('sym'),
                          smoothed_asym.rename('asym')])
    else:
        #-- Persist
        power_bg   = power_bg.chunk('auto').persist()
        power_sym  = power_sym.chunk('auto').persist()
        power_asym = power_asym.chunk('auto').persist()

        power = xr.merge([power_bg.rename('bg'),
                          power_sym.rename('sym'),
                          power_asym.rename('asym')])
    
    #-- Set global attributes
    power.attrs['spd']       = str(spd)
    power.attrs['nDayWin']   = str(nDayWin)
    power.attrs['nDaySkp']   = str(nDaySkp)
    power.attrs['taper']     = taper_name
    power.attrs['norm']      = str(norm)
    power.attrs['lat_range'] = str(lat_range)
    power.attrs['smoothing'] = str(switch_smooth)
    power.attrs['n_smooth']  = str(n_smooth)
    
    
    #-- Save power spectrum
    power.squeeze().to_netcdf(path=f'{str(power_outfile)}.nc')
    
    #==========================================================================
    #                         E N D   F U N C T I O N
    #--------------------------------------------------------------------------
    #-- Print start time
    print('\033[1m\033[92m End:       ' + datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    print('\033[0m-------------------------------------------------------------------------------------')
    print('')


#==============================================================================
# function split_sym(): Splits an equatorial symmetric dataset into its
#                       symmetric and antisymmetric components
#------------------------------------------------------------------------------
def split_sym(data, var):
    for field in ['north', 'south', 'sym', 'asym']:
        if field not in data.keys():
            data[field] = {}

    half_lat = int(data['raw'][var].shape[1] / 2)
    data['north'][var] = data['raw'][var][:, :half_lat     , :]
    data['south'][var] = data['raw'][var][:, :half_lat-1:-1, :]
    
    data['sym' ][var] = (data['north'][var] + data['south'][var]) / 2
    data['asym'][var] = (data['north'][var] - data['south'][var]) / 2
    
    return data


#==============================================================================
#-- function init_taper(): Initializes a taper field according to the shape
#                          of "subset".
#------------------------------------------------------------------------------
def init_taper(subset, nSamWin, taper_name='stull'):
    #-- Initialize taper field
    taper = xr.DataArray(data = np.nan * np.ones((nSamWin,
                                                  subset.sizes['lat'],
                                                  subset.sizes['lon'])),
                         coords = {'time'  : np.arange(0, nSamWin),
                                   'lat'   : subset.lat,
                                   'lon'   : subset.lon},
                         dims   = ('time', 'lat', 'lon'))

    #-- Setup of taper field
    if taper_name == 'stull':
        for i in np.arange(0, nSamWin):
            if (i < 0.1*nSamWin or i > 0.9*nSamWin):
                taper[i, :, :] = (np.sin(5 * np.pi * (i/nSamWin)))**2
            else:
                taper[i, :, :] = 1.
    elif taper_name == 'split_cosine':
        for i in np.arange(0, nSamWin):
            if (i < 0.125*nSamWin):
                taper[i, :, :] = np.cos((-0.5 + 4 *(i/nSamWin)) * np.pi)
            elif (i > 0.875*nSamWin):
                taper[i, :, :] = np.cos((0.5 + 4 *(i/nSamWin)) * np.pi)
            else:
                taper[i, :, :] = 1.
    elif taper_name == 'welch':
        for i in np.arange(0,nSamWin):
            taper[i, :, :] = 1 - ((i-nSamWin/2) / (nSamWin/2))**2
    else:
        print(f'Taper {taper_name} does not exist. Please provide a valid taper.')
    
    return taper

              
#==============================================================================
# function wk121(): Performs a 121 smoothing.
#------------------------------------------------------------------------------
def wk121(data, dim):
    if len(data.shape) == 1:
        #-- Define shifted data fields
        min1 = data[0:-2]
        inst = data[1:-1]
        plu1 = data[2:  ]

        #-- Initialize smoothed data array
        smoothed_mid = inst
        smoothed_1st = data[ 0:1]
        smoothed_3rd = data[-1: ]

        #-- Calculate smoothed data
        smoothed_1st.data = 0.25 * (3*data.data[ 0:1] + data.data[ 1: 2])
        smoothed_3rd.data = 0.25 * (3*data.data[-1: ] + data.data[-2:-1])
        smoothed_mid.data = 0.25 * (min1.data + 2*inst.data + plu1.data)

        #-- Add first and last value
        smoothed = xr.concat([smoothed_1st, smoothed_mid, smoothed_3rd], dim=dim)
    
    elif len(data.shape) == 2:
        if dim == 'k':
            #-- Define shifted data fields
            min1 = data[0:-2, :]
            inst = data[1:-1, :]
            plu1 = data[2:  , :]
            
            #-- Initialize smoothed data array
            smoothed_mid = inst
            smoothed_1st = data[ 0:1, :]
            smoothed_3rd = data[-1: , :]
            
            #-- Calculate smoothed data
            smoothed_1st.data = 0.25 * (3*data.data[ 0:1, :] + data.data[ 1: 2, :])
            smoothed_3rd.data = 0.25 * (3*data.data[-1: , :] + data.data[-2:-1, :])
            smoothed_mid.data = 0.25 * (min1.data + 2*inst.data + plu1.data)
        
        elif dim == 'f':
            #-- Define shifted data fields
            min1 = data[:, 0:-2]
            inst = data[:, 1:-1]
            plu1 = data[:, 2:  ]
            
            #-- Initialize smoothed data array
            smoothed_mid = inst
            smoothed_1st = data[:,  0:1]
            smoothed_3rd = data[:, -1: ]
            
            #-- Calculate smoothed data
            smoothed_1st.data = 0.25 * (3*data.data[:,  0:1] + data.data[:,  1: 2])
            smoothed_3rd.data = 0.25 * (3*data.data[:, -1: ] + data.data[:, -2:-1])
            smoothed_mid.data = 0.25 * (min1.data + 2*inst.data + plu1.data)
            
        #-- Add first and last value
        smoothed = xr.concat([smoothed_1st, smoothed_mid, smoothed_3rd], dim=dim)
        
    elif len(data.shape) == 3:
        if dim == 'k':
            #-- Define shifted data fields
            min1 = data[:, 0:-2, :]
            inst = data[:, 1:-1, :]
            plu1 = data[:, 2:  , :]
            
            #-- Initialize smoothed data array
            smoothed_mid = inst
            smoothed_1st = data[:,  0:1, :]
            smoothed_3rd = data[:, -1: , :]
            
            #-- Calculate smoothed data
            smoothed_1st.data = 0.25 * (3*data.data[:,  0:1, :] + data.data[:,  1: 2, :])
            smoothed_3rd.data = 0.25 * (3*data.data[:, -1: , :] + data.data[:, -2:-1, :])
            smoothed_mid.data = 0.25 * (min1.data + 2*inst.data + plu1.data)
        
        elif dim == 'f':
            #-- Define shifted data fields
            min1 = data[:, :, 0:-2]
            inst = data[:, :, 1:-1]
            plu1 = data[:, :, 2:  ]
            
            #-- Initialize smoothed data array
            smoothed_mid = inst
            smoothed_1st = data[:, :,  0:1]
            smoothed_3rd = data[:, :, -1: ]
            
            #-- Calculate smoothed data
            smoothed_1st.data = 0.25 * (3*data.data[:, :,  0:1] + data.data[:, :,  1: 2])
            smoothed_3rd.data = 0.25 * (3*data.data[:, :, -1: ] + data.data[:, :, -2:-1])
            smoothed_mid.data = 0.25 * (min1.data + 2*inst.data + plu1.data)
            
        #-- Add first and last value
        smoothed = xr.concat([smoothed_1st, smoothed_mid, smoothed_3rd], dim=dim)
    
    return smoothed


#==============================================================================
# function print_fft_settings(): Prints user chosen settings for spectral
#                                analysis.
#==============================================================================
def print_fft_settings(settings):
    print('-------------------------------------------------------------------------------------')
    print('\033[1m\033[92m Done:     ' + datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    print('')
    print(f'\033[0m var = \033[1m\033[91m{settings["var"]}')
    print(f'\033[0m lat_range = \033[1m\033[91m{settings["lat_range"]}')
    print(f'\033[0m spd = \033[1m\033[91m{settings["spd"]}')
    print(f'\033[0m nDayWin = \033[1m\033[91m{settings["nDayWin"]}')
    print(f'\033[0m nDaySkp = \033[1m\033[91m{settings["nDaySkp"]}')
    print(f'\033[0m taper = \033[1m\033[91m{settings["taper_name"]}')
    print('\033[0m-------------------------------------------------------------------------------------')
    print('')
