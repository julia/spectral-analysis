# Installation of Spectral Analysis Pipeline
``` bash
cd ~
module add gcc/7.1.0 julia/1.5.2-gcc-9.1.0
git clone https://gitlab.dkrz.de/julia/spectral-analysis
cd spectral-analysis
julia --project=@. -e "import Pkg; Pkg.instantiate()"
```

# Configure
The project is configured at the end of the spc_analysis.jl file.

# Execute
``` bash
cd spectral-analysis
julia --project=. spc_analysis.jl 
```