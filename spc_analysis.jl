#----------------------------------------------------------------------------
# Developed by Volker Neff based on a Python script by Henning Franke
# February 2021
#----------------------------------------------------------------------------


using FFTW
using NCDatasets
using Statistics
import Base.OneTo

#----------------------------------------------------------------------------
#               P E R F O R M   F F T   A N A L Y S I S
#----------------------------------------------------------------------------

abstract type AbstractTaper end

struct Stull <: AbstractTaper
    data::Array{Float64, 3}
end
function Stull(nSamWin, lat, lon)
    data = Array{Float64, 3}(undef, lon, lat, nSamWin)

    for i in OneTo(nSamWin)
        if i < (0.1 * nSamWin) + 1 || i > 0.9 * nSamWin
            data[:, :, i] .= (sin(5 * pi * (i / nSamWin)))^2
        else
            data[:, :, i] .= 1
        end
    end

    return Stull(data)
end


#----------------------------------------------------------------------------
#                    H E L P E R   F U N C T I O N S
#----------------------------------------------------------------------------
function removeMissing(input)
    s = size(input)
    rodata = collect(skipmissing(input))
    return reshape(rodata, s...)
end

function writeComplex(input)
    out = Array{Float32, 4}(undef, 2, size(input)...)
    out[1, :, :, :] .= real(input)
    out[2, :, :, :] .= imag(input)
    return out
end

function readComplex(input)
    input[2, :, :, :]*im + input[1, :, :, :]
end

function create_output_file(path, lat, f_fft_cord, k_fft_cord; comp=false)
    ds = NCDataset(path, "c")

    defDim(ds, "f_fft", length(f_fft_cord))
    defDim(ds, "lat", length(lat)) #length(inputDataset["lat"]))
    defDim(ds, "k_fft", length(k_fft_cord))
    if comp
        defDim(ds, "ReIm", 2)
    end

    f_fft = defVar(ds, "f_fft", Int64, ("f_fft",),
        attrib = Dict(
            "standard_name" => "wave frequency",
            #"long_name" = "wave frequency",
            "unit" => "--",
            "axis" =>  "T"
        ))
    f_fft[:] = f_fft_cord

    lat = defVar(ds, "lat", Int64, ("lat",), attrib = lat.attrib) #inputDataset["lat"].attrib)
    lat[:] = lat[:]

    k_fft = defVar(ds, "k_fft", Int64, ("k_fft",), attrib = Dict(
        "standard_name" => "zonal wave number",
        #"long_name" = "zonal wave number",
        "unit" => "--",
        "axis" =>  "X"
    ))
    k_fft[:] = k_fft_cord

    if comp
        fft_coeff_ta = defVar(ds, "fft_coeff_ta", Float64, ("ReIm", "k_fft", "lat", "f_fft"))
    else
        fft_coeff_ta = defVar(ds, "fft_coeff_ta", Float64, ("k_fft", "lat", "f_fft"))
    end

    return ds, fft_coeff_ta
end

#------------------------------------------------------------------------------
# function mer_weight(): Calculates the weights for meridional averaging
#------------------------------------------------------------------------------
function mer_weight_create(mer_res)
    #-- Calculate latitudinal step width of grid
    step = pi / mer_res

    #-- Initialize lat_bnds
    lat_bnds = collect([(i - 1) * step for i in OneTo(mer_res+1)])

    R_earth = 6357000
    lat_bnds = (2*pi*R_earth^2).*(1 .- cos.(lat_bnds))

    lat_bnds = lat_bnds[2:end] - lat_bnds[begin:end-1]

    return lat_bnds
end

#----------------------------------------------------------------------------
#               S P E C T R A L   A N A L Y S E
#----------------------------------------------------------------------------

"""
spectral_analyse

# Parameter:
- `inputDataset`: cdi.jl Dataset
- `var::AbstractString`: Name of the variable that should be compute
- `dimension::AbstractString`: 'f', 'k' or 'fk'
- `spd`: samples per day
- `norm`: Should be the fft normalized?
"""
function spectral_analyse(inputDataset::NCDataset;
    var::AbstractString,
    dimension::AbstractString,
    lat_range::UnitRange,
    spd,
    nDayWin,
    nDaySkp,
    taper_name,
    norm::Bool,
    mer_weight,
    switch_smooth,
    fft_outpath::AbstractString,
    power_outpath::AbstractString
)
    @info "start function"

    # Calculate the number of timestaps
    timesteps = length(inputDataset["time"])

    #-- Calculate WK parameters out of input parameters
    nDayTot = timesteps / spd                   # Total number of days in dataset
    nSamTot = nDayTot * spd                     # Total number of samples in dataset
    nSamWin = nDayWin * spd                     # Number of samples per window
    nSamSkp = nDaySkp * spd                     # Number of samples to skip between windows
    nWindow = Int((nSamTot-nSamWin)/(nSamWin+nSamSkp) + 1) #-- Total amount of windows

    #-- Shorten daily windows if sample is shorter than nDayWin
    if nDayTot < nDayWin
        nDayWin = timesteps / spd
    end

    #-- Critical frequency: Remove all contributions with a lower frequency
    fCrit = 1/nDayWin

    #-- Print spectral analysis settings
    println("nDayTot = $(nDayTot)")
    println("nSamTot = $(nSamTot)")
    println("nDayWin = $(nDayWin)")
    println("nSamWin = $(nSamWin)")
    println("nDaySkp = $(nDaySkp)")
    println("nSamSkp = $(nSamSkp)")
    println("nWindow = $(nWindow)")
    println("fCrit = $(fCrit)")
    println("taper = $(taper_name)")
    println("norm = $(norm)")

    #----------------------------------------------------------------------------
    #                            D E T R E N D I N G
    #----------------------------------------------------------------------------

    # Currently not possible ;(
        #-- Remove LONG TERM linear trend in whole dataset. But KEEP the mean!
    # attrs  = subset.attrs
    # subset = subset.reduce(sig.detrend, axis=0, type='linear', allow_lazy=True,
    #                       keep_attrs=True) + \
    #                        subset.mean(dim='time', keep_attrs=True)

    # -- Remove annual cyle if dataset is longer than two years
    if nDayTot >= 365
        # TBD: some function to remove annual cycle...
        error("Currently a function to remove annual cycle doesn't exists! :(")
    end

    #----------------------------------------------------------------------------
    #               P E R F O R M   F F T   A N A L Y S I S
    #----------------------------------------------------------------------------

    # TODO different taper
    tapper = nothing
    if taper_name == "stull"
        taper = Stull(nSamWin, length(inputDataset["lat"]), length(inputDataset["lon"])) # define lat / lon
    else
        @error "Tapper not supported"
    end

    #-- Set start and end time
    t_start = 1
    t_end   = nDayWin * spd

    f_fft_cord = sort([collect(Int64, -Int(nSamWin/2)+1:Int(nSamWin/2))...])
    k_fft_cord = sort([collect(Int64, -Int(length(inputDataset["lon"])/2)+1:Int(length(inputDataset["lon"])/2))...])

    power_avg = nothing

    if dimension == "fk"
        for nw in OneTo(nWindow)

            println("FFT for window " * string(nw))

            fft_output_path = join([fft_outpath, "_window", string(nw), ".nc"])
            if !isfile(fft_output_path) || true# file exist

                ds, fft_coeff_ta = create_output_file(fft_output_path, inputDataset["lat"], f_fft_cord, k_fft_cord, comp=true)

                #----------------------------------------------------------------------------
                #               P E R F O R M   F F T   A N A L Y S I S
                #----------------------------------------------------------------------------

                #-- Create working dataset
                workset = removeMissing(inputDataset[var][:, :, 1, t_start:t_end])

                println("Sum: ", sum(workset))

                #-- Temporal detrending
                # Currently not possible ;(

                #-- Tapering
                @info size(taper.data)
                @. workset = workset * taper.data
                @info typeof(workset)
                println("Sum: ", sum(workset))

                fft_data = fft(workset, [1, 3])
                @info typeof(fft_data)
                println("Sum: ", sum(fft_data))
                workset = nothing
                if norm
                    fft_data .= fft_data ./ (nSamWin * length(inputDataset["lon"]))
                end

                # save the file
                @info "Write $fft_output_path"
                fft_coeff_ta[:, :, :, :] = writeComplex(fft_data)
            else
                # load the file
            end

            #-- Set start and end index for next window
            t_start = t_end   + nSamSkp
            t_end   = t_start + nSamWin

             #-------------------------------------------------------------------------
             #                       P O W E R _  A V G  
             #-------------------------------------------------------------------------

            power_output_path = join([power_outpath, "_squ_avg_fft.nc"])
            if !isfile(power_output_path) || true
                # write power avg
                power_avg = Array{Float64, 3}(undef, size(fft_data)...)
                r = real(fft_data)
                i = imag(fft_data)
                @. power_avg = 1 / nWindow * (r^2 + i^2)
                power_avg = permutedims(power_avg, [3,2,1])

                #----------------------------------------------------------------------------
                #           I N I T I A L I Z E   O U T P U T   F I L E
                #----------------------------------------------------------------------------
                ds, spc_power_squ_avg_fft = create_output_file(power_output_path, inputDataset["lat"], f_fft_cord, k_fft_cord)

                spc_power_squ_avg_fft = defVar(ds, "spc_power_squ_avg_fft", Float64, ("f_fft", "lat","k_fft"))

                @info "Write $power_output_path"
                spc_power_squ_avg_fft[:, :, :] = power_avg;
            else
                @info "Read $power_output_path"
                ds = NCDataset(power_output_path)
                power_avg = ds["spc_power_squ_avg_fft"][:, :, :]
            end

        end

    end

    #--------------------------------------------------------------------------
    #       C A L C U L A T E   P O W E R   S P E C T R U M
    #--------------------------------------------------------------------------

    power_avg_size = size(power_avg)
    power = zeros(Float64, power_avg_size[1] + 1, power_avg_size[2], power_avg_size[3]+1)
    if dimension == "fk"
        #-- Set indexing variables for faster access
        mlon      = length(inputDataset["lon"])
        mlon_2    = convert(Int64, floor(mlon/2))
        nSamWin_2 = convert(Int64, floor(nSamWin/2))

        @info "Size(power_avg) " * string(size(power_avg))
        @info "Size(power) " * string(size(power))
        @info mlon
        @info mlon_2
        @info nSamWin_2

        #power[begin:nSamWin_2, :, begin:mlon_2] = power_avg[nSamWin_2+1:end, :, mlon_2:-1:begin]
        power[nSamWin_2+1:end, :, begin:mlon_2] = power_avg[begin:nSamWin_2+1, :, mlon_2+1:-1:begin+1] # ok

        #power[begin:nSamWin_2, :, mlon_2+2:end] = power_avg[nSamWin_2+1:end, :, mlon_2+1:end]
        power[nSamWin_2+1:end, :, mlon_2+1:end] = reverse(power_avg[nSamWin_2:end, :, begin:mlon_2+1], dims=1) # ok


        println("Sum ABC")
        println(sum(power))

        #-- Remove negative frequencies and set the mean (f=0) to missing
        power = power[nSamWin_2+1:end, :, :]
        power = map(x -> if x == 0; NaN; else x; end, power)
    end

    @info "sum power"
    @info sum(reshape(map(x -> if !isnan(x); x; else; 0.0; end, power), :))
    @info sum(power_avg)

    @. power = power * mer_weight
    power = mean(power, dims=2)[:, 1, :]

    #-- Further averaging for 1d power spectra
    if dimension == "f"
    elseif dimension == "k" 
    end

    #--------------------------------------------------------------------------
    #                    W K - 1 2 1   S M O O T H I N G 
    #--------------------------------------------------------------------------
    if switch_smooth
    end

    #----------------------------------------------------------------------------
    #           I N I T I A L I Z E   O U T P U T   F I L E
    #----------------------------------------------------------------------------
    power_output_path = join([power_outpath, ".nc"])
    ds = NCDataset(power_output_path, "c")

    defDim(ds, "f", size(power)[1])
    defDim(ds, "k", size(power)[2])

    f_fft = defVar(ds, "f", Float32, ("f",),
        attrib = Dict(
            "standard_name" => "wave frequency",
            #"long_name" = "wave frequency",
            "unit" => "--",
            "axis" =>  "T"
        ))
    f_fft[:] = collect(0:length(f_fft_cord)÷2) / nDayWin

    k_fft = defVar(ds, "k", Float32, ("k",), attrib = Dict(
        "standard_name" => "zonal wave number",
        #"long_name" = "zonal wave number",
        "unit" => "--",
        "axis" =>  "X"
    ))
    k_fft[:] = collect(-(length(k_fft_cord)÷2):length(k_fft_cord)÷2)

    spc_power_squ_avg_fft = defVar(ds, "power_ta", Float64, ("f", "k"); attrib = Dict("_FillValue" => NaN))

    @info "Write $power_output_path"
    spc_power_squ_avg_fft[:, :] = power;

end

@info "start program"

ds = NCDataset("data/out2.nc")

mer_weight = mer_weight_create(512)
b =  convert(Int64, floor(512/2-length(ds["lat"])/2)+1)
e =  convert(Int64, floor(512/2+length(ds["lat"])/2))
mer_weight = mer_weight[b:e]
m = mean(mer_weight)
@info mer_weight

mer_weight = mer_weight ./ mean(mer_weight)
mer_weight = reshape(mer_weight, 1, length(ds["lat"]), 1)

spectral_analyse(
    ds, 
    var="ta", 
    dimension = "fk",
    lat_range = -15:15,
    spd = 24,
    nDayWin = 30,
    nDaySkp = 0,
    taper_name = "stull",
    norm = true,
    mer_weight = mer_weight,
    switch_smooth = false,
    fft_outpath = "output/vn_fft_",
    power_outpath = "output/vn_power_"
)