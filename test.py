import xarray            as xr
import numpy             as np

import hf_functions    as hffun
import hf_spc_analysis as hfspc

#-- Modify settings for spectral analysis
spc_settings = {'var'        : "ta",           #-- Varible to perform spectral analysis on
                'dimension'  : "fk",           #-- Dimension on which to perform FFT along
                'lat_range'  : (-15.0, 15.0),  #-- Set lat-height range to subsample
                'spd'        : 24,             #-- Samples per day
                'nDayWin'    : 30,             #-- Window length in days
                'nDaySkp'    : 0,              #-- Length in between temporal windows. Negative corresponds to overlapping
                'taper_name' : 'stull', #'split_cosine', #-- Taper function
                'norm'       : True}           #-- Normalization of FFT output by Number of samples

fft_outfile = "output/fft_"
pow_outfile = "output/power_"

#-- Resolution
res = [1024, 512]

#-- Modify settings of smoothing for power spectra
switch_smooth = False
n_smooth      = {'k' : [5, 10, 20, 40],
                 'f' : 10}

dset = xr.open_mfdataset("data/out2.nc", concat_dim = 0, combine = 'by_coords')

#-- Assign meridional weight
mer_weight = hffun.mer_weight(res[1])
mer_weight = mer_weight[int(res[1]/2-dset['lat'].shape[0]/2):
                        int(res[1]/2+dset['lat'].shape[0]/2)]
print(mer_weight)
mer_weight = mer_weight / mer_weight.mean()
mer_weight = mer_weight.reshape([1, dset['lat'].shape[0], 1])


#-- Subsampling
dset = dset[spc_settings['var']]

#-- Change units
dset = hffun.change_unit(dset, spc_settings['var'], target_unit = None)

 #-- Subsample height
fft_set = dset.squeeze().chunk('auto')
fft_set = fft_set.persist()

hfspc.spc_analysis(spc_settings, fft_set, fft_outfile, pow_outfile, mer_weight, switch_smooth, n_smooth)