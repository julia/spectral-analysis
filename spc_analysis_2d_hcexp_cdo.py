# #### Program: WK_spec_analysis_2d_hc_experiments.py
# #### Author: Henning Franke
# #### Version: 15.12.2020


#====================================================================================
#                          I M P O R T   P A C K A G E S
#====================================================================================
#-- Packages for path, file, and time management
from getpass  import getuser
from pathlib  import Path
from tempfile import NamedTemporaryFile, TemporaryDirectory
from datetime import datetime

#-- Packages to prevent printing of warnings
import warnings
warnings.filterwarnings(action='ignore')
import importlib as implib

#-- Packages for data handling and plotting
import xarray            as xr
import numpy             as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.dates  as mdates

#-- Packages for HPC computation
from dask.utils       import format_bytes
from dask.distributed import Client, progress
from dask.diagnostics import ProgressBar
from distributed.deploy.local import LocalCluster
from dask_jobqueue import SLURMCluster
import dask.array as dr

#-- Increase python path
import sys
import os
sys.path.append(str(Path.home() / Path('python/lib')))
sys.path.append(str(Path.home() / Path('python/spectral_analysis/py_code/lib')))

#-- Selfwritten packages
import hf_functions    as hffun
import hf_plotting     as hfplt
import hf_definitions  as hfdef
import hf_spc_analysis as hfspc

#-- Print statement
hffun.print_done()


#====================================================================================
#           M A N A G E   C O M M A N D - L I N E   A R G U M E N T S
#====================================================================================
var = sys.argv[1]
dim = sys.argv[2]
wk_switch = bool(int(sys.argv[3]))
if wk_switch:
    wk = '_WK'
elif not wk_switch:
    wk = ''


#====================================================================================
#        U S E R   S E T T I N G S:   O N L Y   C E L L   T O   M O D I F Y
#====================================================================================
#-- Experiment specifications
exp_lst  = ['hc11_02_p1m_post_n256',
            'hc12_02_p1m_post_n256',
            'hc13_02_p1m_post_n256',
            'hc14_02_p1m_post_n256',
            'hc2_02_p1m_post_n256',
            'hc3_02_p1m_post_n256',
            'hc4_02_p1m_post_n256']
file_var = ['atm2d',
            'flx2d',
            'flx2d',
            'flx2d',
            'atm2d',
            'atm2d',
            'atm2d']

#-- Modify settings for spectral analysis
spc_settings = {'var'        : var,           #-- Varible to perform spectral analysis on
                'dimension'  : dim,           #-- Dimension on which to perform FFT along
                'lat_range'  : (-15.0, 15.0),  #-- Set lat-height range to subsample
                'spd'        : 24,             #-- Samples per day
                'nDayWin'    : 30,             #-- Window length in days
                'nDaySkp'    : 0,              #-- Length in between temporal windows. Negative corresponds to overlapping
                'taper_name' : 'split_cosine', #-- Taper function
                'norm'       : True}           #-- Normalization of FFT output by number of samples

#-- Switch for how to detrend the data fields. First element indicates if data should be
#   detrended in general, second element indicates whether mean should be kept.
dtrend = {'temporal': (True, False),
          'zonal'   : (False, False)}

#-- Modify settings of smoothing for power spectra
switch_smooth = False
n_smooth      = {'k' : [5, 10, 20, 40],
                 'f' : 10}

#-- Resolution
res = [1024, 512]

#-- Output path for .nc files containing FFT coeffcicients and power spectra
outpath_spectra = Path(f'/scratch/snx3000/hfranke/spectral_data/hc_experiments/initial_analysis/')

#-- Print statement
hfspc.print_fft_settings(spc_settings)


#====================================================================================
#      M A N A G E   I N P U T   A N D   O U T P U T   F I L E   S E T T I N G S
#====================================================================================
#-- Output path for .nc files containing FFT coeffcicients and power spectra
outpath_fft = outpath_spectra / Path(f'{spc_settings["var"]}{wk}/fft_coeff/')
outpath_pow = outpath_spectra / Path(f'{spc_settings["var"]}{wk}/spc_power/')

#-- Create outpath if it's not existent:
if not outpath_fft.exists():
    outpath_fft.mkdir(parents=True)
if not outpath_pow.exists():
    outpath_pow.mkdir(parents=True)

#-- Running index for file accessing
run_idx = 0

#-- Loop over all experiments
for exp in exp_lst:
    #-- Input paths
    inpath = Path('/project/pr55/experiments/' + exp + '/')
    
    #-- Set environment variable to prevent HDF5 error in case of being on /project
    if 'project' in inpath.parts[1]:
        os.environ['HDF5_USE_FILE_LOCKING'] = 'FALSE'
    
    #-- Input files
    if exp == 'hc11_02_p1m_post_n256':
        infile = sorted([str(f) for f in inpath.rglob(f'*p1m_{file_var[run_idx]}_ml_200404*.nc')])
    else:
        infile = sorted([str(f) for f in inpath.rglob(f'*p1m_{file_var[run_idx]}_ml_*.nc')])

    #-- Print statement
    hffun.print_done('MANAGE INPUT AND OUTPUT FILE SETTINGS')
    
    
    #====================================================================================
    #             G E N E R A T E   I N P U T - F I L E S   V I A   C D O
    #====================================================================================
    from cdo import Cdo
    cdo = Cdo('/project/pr55/local.gcc-8.3.0/bin/cdo')

    #-- Initialize outpath and outfile
    outpath_cdo = Path('/scratch/snx3000/hfranke/' + exp + '_hl_spcset/')
    if not outpath_cdo.exists():
        outpath_cdo.mkdir(parents=True)
    outfile_cdo = []

    #-- CDO command
    for file in infile:
        print(f'cdo for {file[48:-3]}')
        outfile_cdo.append(str(outpath_cdo) + f'/{file[48:-3]}_spcset_00.000km.nc')
        if not Path(outfile_cdo[-1]).exists():
            cdo.sellonlatbox(f'0,360,{spc_settings["lat_range"][0]},{spc_settings["lat_range"][1]}',
                             input=file, output=outfile_cdo[-1])


    #====================================================================================
    #              R E A D   D A T A   A N D   P R E P R O C E S S I N G
    #====================================================================================
    #-- Read wrk_var
    dset = xr.open_mfdataset(outfile_cdo, concat_dim = 0, combine = 'by_coords')

    #-- Assign meridional weight
    mer_weight = hffun.mer_weight(res[1])
    mer_weight = mer_weight[int(res[1]/2-dset['lat'].shape[0]/2):
                            int(res[1]/2+dset['lat'].shape[0]/2)]
    mer_weight = mer_weight / mer_weight.mean()
    mer_weight = mer_weight.reshape([1, dset['lat'].shape[0], 1])

    #-- Subsampling
    dset = dset[spc_settings['var']]
    
    #-- Change units
    dset = hffun.change_unit(dset, spc_settings['var'], target_unit = None)

    #-- Print statement
    hffun.print_done(text = 'READ DATA AND PREPROCESSING')


    #====================================================================================
    #           C A L C U L A T I O N   O F   F F T   C O E F F I C I E N T S
    #====================================================================================
    #-- Print statement
    print('')
    print(f'\033[1mCalculate FFT coefficients')

    #-- Set FFT outfile
    fft_outfile = outpath_fft / Path(f'{exp}_{spc_settings["var"]}{wk}_' +
                                     f'{spc_settings["dimension"]}_fft_coeff_00.000km')
    pow_outfile = outpath_pow / Path(f'{exp}_{spc_settings["var"]}{wk}_' +
                                     f'{spc_settings["dimension"]}_spc_power_00.000km')
    
    #-- Subsample height
    fft_set = dset.squeeze().chunk('auto')
    fft_set = fft_set.persist()

    #-- Perform FFT
    if wk_switch:
        print('')
        print(f'\033[1mWHEELER AND KILADIS (1999) SPECTRAL ANALYSIS')
        hfspc.wk_analysis(spc_settings, dtrend, fft_set,
                          fft_outfile, pow_outfile,
                          mer_weight, switch_smooth, n_smooth)
    elif not wk_switch:
        print('')
        print(f'\033[1mNORMAL SPECTRAL ANALYSIS')
        hfspc.spc_analysis(spc_settings, fft_set,
                           fft_outfile, pow_outfile,
                           mer_weight, switch_smooth, n_smooth)
    
    #-- Remove data
    del dset
    del fft_set
    
    #-- Increase running index
    run_idx = run_idx + 1
    
#-- Print statement
hffun.print_done()