#==============================================================================
# PROGRAM: hf_functions.py
# AUTHOR: Henning Franke (m300738)
# CREATION: 10.08.2020
# SHORT DESCRIPTION: This program contains useful functions for many purposes:
#                    a) Reading of data
#                    b) Physical calculations
#                    c) Statistical operations
#------------------------------------------------------------------------------


#==============================================================================
#-- IMPORT PACKAGES
#------------------------------------------------------------------------------
#-- Import packages for calculations
import xarray       as xr
import numpy        as np
import scipy        as sp
import scipy.signal as sig
import scipy.fft    as fft
from sklearn.linear_model import LinearRegression as sklinreg
from datetime import datetime

#-- Import packages for data and path handling
from pathlib import Path
import sys
sys.path.append(str(Path.home() / Path("python/lib")))

#-- Import self-written packages
#import hf_definitions as hfdef


#==============================================================================
# function runningMean(): Calculates the running mean of a timeseries. Written 
#                         by Henning Franke in August 2020.
#------------------------------------------------------------------------------
def runningMean(x, N):
    y = np.zeros((len(x),))
    if N>2:
        z = N/2
        for ctr in np.arange(z, len(x)-z):
            y[int(ctr)] = np.sum(x[int(ctr-z):int(ctr+z)])
        y = y/N
        y[:int(z)]  = np.NaN
        y[int(-z):] = np.NaN
        return y


#==============================================================================
# function running_mean(): Calculates the running mean of a timeseries. Down-
#                          loaded from Stackoverflow.com in August 2020.
#------------------------------------------------------------------------------
def running_mean(x, N):
    cumsum = np.cumsum(np.insert(x, 0, 0)) 
    return (cumsum[N:] - cumsum[:-N]) / float(N)


#==============================================================================
# function linreg(): Calculates the linear regression for a 2dim input and a
#                       2dim output array
#------------------------------------------------------------------------------
def linreg(x, y, return_output=False):
    model  = sklinreg().fit(x.flatten().reshape((-1, 1)), y.flatten())
    a      = model.coef_[0]
    b      = model.intercept_
    R2     = model.score(x.flatten().reshape((-1, 1)), y.flatten())
    output = model.predict(x.flatten().reshape((-1, 1)))
    
    if return_output:
        return a, b, R2, output
    else:
        return a, b, R2


#==============================================================================
# function rms(): Calculates the root mean square (rms) of an input field
#------------------------------------------------------------------------------
def rms(x):
    rms = np.sqrt(np.mean(np.square(x)))
    
    return rms

#==============================================================================
# function tpcor(): Calculates the two-point correlation for two fields X and Y
#                   without a spatial lag
#------------------------------------------------------------------------------
def tpcor(X,Y):
    tpcor = np.mean(X * Y) / (rms(X) * rms(Y))
    
    return tpcor


#==============================================================================
# function t_pot(): Calculates the potential temperature at a given pressure 
#                   level for a given temperature
#------------------------------------------------------------------------------
def t_pot(temp, pres, p_norm=1000.00):
    temp_data = temp.data
    pres_data = pres.data
    temp_unit = temp.attrs['units']
    pres_unit = pres.attrs['units']
    
    #-- Change units to SI-units
    if pres_unit == 'Pa':
        pres_data = pres_data / 100
    
    #-- Calculation
    temp_pot = temp_data * (p_norm/pres_data)**(hfdef.R_L/hfdef.c_p)
    
    return temp_pot


#==============================================================================
# function mer_grad(): Calculates the meridional gradient of a multidimensional
#                      numpy array 'data', where the meridional dimension is 
#                      stored in dimension 'axis' (default axis=0).
#------------------------------------------------------------------------------
def mer_grad(data, axis=0):
    #-- Set constants
    n_lon = 64       # Amount of meridional gridboxes (meridional resolution)
    
    #-- Calculate meridional length of grid box
    grid_len = (hfdef.R_earth * 2 * np.pi) * ((180/n_lon) / 360)
    
    #-- Calculate gradient
    grad = np.gradient(data, grid_len, axis=axis)
    
    #Docu np.gradient: The gradient is computed using second order accurate 
    #central differences in the interior points and either first or second 
    #order accurate one-sides (forward or backwards) differences at the 
    #boundaries. The returned gradient hence has the same shape as the input
    #array.
    
    #-- Conversion of unit from K/m to K/100km
    grad_1 = grad * 100000
    
    return grad_1


#==============================================================================
# function date_str(): Creates nice datestrings out og np.datetime64 objects.
#------------------------------------------------------------------------------
def date_str(np_date, form = 'default'):
    year   = str(np_date)[ 0: 4]
    month  = str(np_date)[ 5: 7]
    day    = str(np_date)[ 8:10]
    hour   = str(np_date)[11:13]
    minute = str(np_date)[14:16]
    
    datestr = {'default' : year + '-' + month + '-' + day + ' ' + hour + ':' + 
                           minute + 'Z',
               'US'      : year + '/' + month + '/' + day + ' ' + hour + ':' + 
                           minute + 'Z',
               'EU'      : day + '.' + month + '.' + year + ' ' + hour + ':' + 
                           minute + 'Z',
               'save'    : year + month + day + 'T' + hour + minute + 'Z'
              }
    
    return datestr.get(form, "Invalid format")

#==============================================================================
# function mer_weight(): Calculates the weights for meridional averaging
#------------------------------------------------------------------------------
def mer_weight(mer_res):
    #-- Calculate latitudinal step width of grid
    step = np.pi / mer_res

    #-- Initialize lat_bnds
    lat_bnds = [0]

    #-- Set lat_bnds for whole grid and convert to np.array
    while (len(lat_bnds) < mer_res+1): lat_bnds.append(lat_bnds[-1] + step)
    np.array(lat_bnds)

    #-- Calculate the size of the sphere fractions
    R_earth = 6357000 # added by Volker
    lat_bnds = (2*np.pi*R_earth**2)*(1-np.cos(lat_bnds))

    #-- Calculate differences to get area of slices
    lat_bnds = lat_bnds[1:] - lat_bnds[:-1]

    return lat_bnds


#==============================================================================
# function save_complex(): Splits dataset into real and imaginary part.
#------------------------------------------------------------------------------
def save_complex(dataset, *args, **kwargs):
    ds = dataset.expand_dims('ReIm', axis=-1) # Add ReIm axis at the end
    ds = xr.concat([ds.real, ds.imag], dim='ReIm')
    return ds.to_netcdf(*args, **kwargs)


#==============================================================================
# function read_complex(): Reads a complex dataset saved by save_complex
#------------------------------------------------------------------------------
def read_complex(*args, **kwargs):
    ds = xr.open_mfdataset(*args, **kwargs)
    return ds.isel(ReIm=0) + 1j * ds.isel(ReIm=1)


#==============================================================================
# function pres2height(): Converts pressure to height levels.
#==============================================================================
def pres2height(pres, scale=7, norm=1013.25):
    height = -scale * (np.log(pres) - np.log(norm))
    return height


#==============================================================================
# function height2pres(): Converts height to pressure levels.
#==============================================================================
def height2pres(height, scale=7, norm=1013.25):
    pres = norm * (np.exp(height/-scale))
    return pres


#==============================================================================
# function print_done(): Prints time when cell was ran successfully.
#==============================================================================
def print_done(text=''):
    #-- Print statement
    print('-------------------------------------------------------------------------------------')
    print('\033[1m\033[92m Done:     ' + text + '    ' + datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    print('\033[0m-------------------------------------------------------------------------------------')
    print('')


#==============================================================================
# function change_unit(): Changes the unit of an input data field.
#==============================================================================
def change_unit(data, var, target_unit):
    if target_unit:
        if var == 'height':
            #-- Set multiplication factor
            if target_unit == 'km':
                multi = 0.001

            #-- Change units: Height
            attrs = data['height'].attrs
            data  = data.assign_coords(height=(data['height'] * multi))
            data['height'].attrs = attrs
            data['height'].attrs['units'] = 'km'

        else:
            #-- Set multiplication factor
            if var == 'wa':
                if target_unit == 'cm s-1':
                    multi = 100.
                elif target_unit == 'mm s-1':
                    multi = 1000.

                    #-- Set multiplication factor
            if var == 'pr':
                if target_unit == 'mm day-1':
                    multi = 3600. * 24. * (1. / hfdef.rho_H2O)

            #-- Perform unit transformation
            attrs = data.attrs
            data  = data * multi
            data.attrs = attrs
            data.attrs['units'] = target_unit
    
    else:
        print(f'\033[1m{var}: No unit change!')
    
    return data